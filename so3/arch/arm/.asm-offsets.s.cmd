cmd_arch/arm/asm-offsets.s := arm-linux-gnueabihf-gcc -Wp,-MD,arch/arm/.asm-offsets.s.d  -Iinclude  -I.  -include include/generated/autoconf.h -D__KERNEL__ -D__SO3__ -Iinclude -fno-builtin -ffreestanding -nostdinc -isystem /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include -g -O0 -fno-common -marm -mno-thumb-interwork -march=armv7-a -Wall -Wstrict-prototypes -D__KERNEL__ -D__SO3__ -Iinclude -fno-builtin -ffreestanding -nostdinc -isystem /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include -I. -I./lib/libfdt  -Iarch/arm/include/ -Iarch/arm/beaglebone/include/	  -fverbose-asm -S -o arch/arm/asm-offsets.s arch/arm/asm-offsets.c

source_arch/arm/asm-offsets.s := arch/arm/asm-offsets.c

deps_arch/arm/asm-offsets.s := \
  arch/arm/include/asm/processor.h \
    $(wildcard include/config/so3virt.h) \
  include/types.h \
  arch/arm/include/asm/types.h \
  include/compiler.h \
  include/common.h \
  include/linker.h \
  include/printk.h \
  /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include/stdarg.h \
  include/thread.h \
  arch/arm/include/asm/memory.h \
    $(wildcard include/config/ram/base.h) \
  include/list.h \
  include/prefetch.h \
  include/schedule.h \

arch/arm/asm-offsets.s: $(deps_arch/arm/asm-offsets.s)

$(deps_arch/arm/asm-offsets.s):
