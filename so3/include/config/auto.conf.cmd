deps_config := \
	mm/Kconfig \
	apps/Kconfig \
	devices/intc/Kconfig \
	devices/timer/Kconfig \
	devices/serial/Kconfig \
	devices/Kconfig \
	kernel/Kconfig \
	arch/arm/beaglebone/Kconfig \
	arch/arm/vexpress/Kconfig \
	arch/arm/Kconfig \
	Kconfig

include/config/auto.conf: \
	$(deps_config)


$(deps_config): ;
