/*
 * irq.h
 */

#ifndef IRQ_H
#define IRQ_H

#include <common.h>
#include <thread.h>

#include <device/fdt/fdt.h>

/* Maximum physical interrupts than can be managed by SO3 */
#define NR_IRQS 		128

#ifdef CONFIG_SO3VIRT

#define NR_VIRQS              NR_IRQS

#endif /* CONFIG_SO3VIRT */

typedef enum {
	IRQ_COMPLETED = 0,
	IRQ_BOTTOM
} irq_return_t;

typedef irq_return_t(*irq_handler_t)(int irq, void *data);

typedef struct irqdesc {
	irq_handler_t action;

	/* Deferred action */
	irq_handler_t irq_deferred_fn;
	bool deferred_pending;
	bool thread_active;

	/* Private data */
	void *data;

} irqdesc_t;

extern bool __in_interrupt;

extern int arch_irq_init(void);
extern void setup_arch(void);

/* IRQ controller */
typedef struct  {

    void (*irq_enable)(unsigned int irq);
    void (*irq_disable)(unsigned int irq);
    void (*irq_mask)(unsigned int irq);
    void (*irq_unmask)(unsigned int irq);
    void (*irq_handle)(void);

} irq_ops_t;

extern irq_ops_t irq_ops;

int irq_process(uint32_t irq);

void irq_init(void);

irqdesc_t *irq_to_desc(uint32_t irq);

void irq_mask(int irq);
void irq_unmask(int irq);

void irq_bind(int irq, irq_handler_t handler, irq_handler_t irq_deferred_fn, void *data);
void irq_unbind(int irq);

#endif /* IRQ_H */
