/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */


#ifndef DM_TIMER_H
#define DM_TIMER_H

#include <types.h>

#define CLK_32KHZ			32768ull   /* 32.768 kHz */	// used with de timer0
#define CLK_M_OSC			25000000ull   /* 25 MHz */

#define TIMER_OVERFLOW		0xffffffffull   		//0xffffffff


#define FORCE_IDLE						(0 << 2)
#define NO_IDLE							(1 << 2)
#define SMART_IDLE						(2 << 2)
#define SMART_IDLE_WAKEUP_CAPABLE		(3 << 2)


#define MAT_EN_FLAG		(1 << 0)
#define OVF_EN_FLAG		(1 << 1)
#define TCAR_EN_FLAG	(1 << 2)


#define TCLR_ST				(1 << 0)		/* 0x1 : start, 0x0 : stop*/
#define TCLR_AR				(1 << 1)		/* 0x1 : auto-reload, 0x0 : oneshot*/
#define TCLR_OS				(0 << 1)
#define TCLR_PTV_2			(0 << 2)		/* timer divided by 2 */
#define TCLR_PTV_4			(1 << 2)		/* timer divided by 4 */
#define TCLR_PTV_8			(2 << 2)		/* timer divided by 8 */
#define TCLR_PTV_16			(3 << 2)		/* timer divided by 16 */
#define TCLR_PTV_32			(4 << 2)		/* timer divided by 32 */
#define TCLR_PTV_64			(5 << 2)		/* timer divided by 64 */
#define TCLR_PTV_128		(6 << 2)		/* timer divided by 128 */
#define TCLR_PTV_256		(7 << 2)		/* timer divided by 256 */
#define TCLR_PRE			(1 << 5)		/* Prescaler enabler. 0x1 :enabled, timer divided by ptv, 0x0 : disabled, timer divided by 1 */

#define TCLR_CE				(1 << 6)		/* 0x1 : compare mode enabled, 0x0 : compare mode disabled*/
#define TCLR_SCPWM			(1 << 7)
#define TCLR_TCM_NO_CPT		(0 << 8)
#define TCLR_TCM_CPT_LOW	(1 << 8)
#define TCLR_TCM_CPT_HIGH	(2 << 8)
#define TCLR_TCM_CPT_BOTH	(3 << 8)


#define TIMER_LOAD      0x00
#define TIMER_VALUE     0x04
#define TIMER_CTRL      0x08
#define TIMER_CTRL_ONESHOT      (1 << 0)
#define TIMER_CTRL_32BIT        (1 << 1)
#define TIMER_CTRL_DIV1         (0 << 2)
#define TIMER_CTRL_DIV16        (1 << 2)
#define TIMER_CTRL_DIV256       (2 << 2)
#define TIMER_CTRL_IE           (1 << 5)        /* Interrupt Enable (versatile only) */
#define TIMER_CTRL_PERIODIC     (1 << 6)
#define TIMER_CTRL_ENABLE       (1 << 7)

#define TIMER_ACK_IRQ			(0 << 0)

/* Bits and regs definitions */
/* System controller (SP810) register definitions */
#define SP810_TIMER0_ENSEL	(1 << 15)
#define SP810_TIMER1_ENSEL	(1 << 17)
#define SP810_TIMER2_ENSEL	(1 << 19)
#define SP810_TIMER3_ENSEL	(1 << 21)

struct dm_timer {
	volatile u32 tidr[4];		/* 0x00 */
	volatile u32 tiocp_cfg[4];	/* 0x10 */
	volatile u32 irq_eoi;		/* 0x20 */
	volatile u32 irqstatus_raw;	/* 0x24 */
	volatile u32 irqstatus;		/* 0x28 */
	volatile u32 irqenable_set;	/* 0x2C */
	volatile u32 irqenable_clear;/* 0x30 */
	volatile u32 irqwakeen;		/* 0x34 */
	volatile u32 tclr;			/* 0x38 */
	volatile u32 tcrr;			/* 0x3C */
	volatile u32 tldr;			/* 0x40 */
	volatile u32 ttgr;			/* 0x44 */
	volatile u32 twps;			/* 0x48 */
	volatile u32 tmar;			/* 0x4C */
	volatile u32 tcar1;			/* 0x50 */
	volatile u32 tsicr;			/* 0x54 */
	volatile u32 tcar2;			/* 0x58 */
};

struct dm_timer_1ms {
	volatile u32 tidr[4];		/* 0x00 */
	volatile u32 tiocp_cfg;		/* 0x10 */
	volatile u32 tistat;		/* 0x14 */
	volatile u32 tisr;			/* 0x18 */
	volatile u32 tier;			/* 0x1C */
	volatile u32 twer;			/* 0x20 */
	volatile u32 tclr;			/* 0x24 */
	volatile u32 tcrr;			/* 0x28 */
	volatile u32 tldr;			/* 0x2C */
	volatile u32 ttgr;			/* 0x30 */
	volatile u32 twps;			/* 0x34 */
	volatile u32 tmar;			/* 0x38 */
	volatile u32 tcar1;			/* 0x3C */
	volatile u32 tsicr;			/* 0x40 */
	volatile u32 tcar2;			/* 0x44 */
	volatile u32 tpir;			/* 0x48 */
};


struct cm_dpll{
	volatile u32 not_used1;				/* 0x00 */
	volatile u32 clksel_timer7_cl;		/* 0x04 */
	volatile u32 clksel_timer2_cl;		/* 0x08 */
	volatile u32 clksel_timer3_cl;		/* 0x0C */
	volatile u32 clksel_timer4_cl;		/* 0x10 */
	volatile u32 cm_mac_clksel;			/* 0x14 */
	volatile u32 clksel_timer5_cl;		/* 0x18 */
	volatile u32 clksel_timer6_cl;		/* 0x1C */
	volatile u32 cm_cpts_rft_clksel;	/* 0x20 */
	volatile u32 not_used2;				/* 0x24 */
	volatile u32 clksel_timer1ms_clk;	/* 0x28 */
	volatile u32 clksel_gfx_fclk;		/* 0x2C */
	volatile u32 clksel_pru_icss_ocp_clk;/* 0x30 */
	volatile u32 clksel_lcdc_pixel_clk;	/* 0x34 */
	volatile u32 clksel_wdt1_clk;		/* 0x38 */
	volatile u32 clksel_gpio0_dbclk;		/* 0x3C */
};

#endif /* DM_TIMER_H */
