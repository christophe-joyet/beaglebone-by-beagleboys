/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */

#ifndef SUN4I_TIMER_H
#define SUN4I_TIMER_H

#include <types.h>

#define TIMER_RATE		24000000  /* This is the frequency of the timer [Hz] */

#define TIMER_CTRL_ENABLE       (1 << 0)
#define TIMER_CTRL_DISABLE      ~TIMER_CTRL_ENABLE
#define TIMER_CTRL_RELOAD		(1 << 1)
#define TIMER_CTRL_DIV1         (0 << 4)
#define TIMER_CTRL_DIV2         (1 << 4)
#define TIMER_CTRL_DIV4         (2 << 4)
#define TIMER_CTRL_DIV8         (3 << 4)
#define TIMER_CTRL_DIV16        (4 << 4)
#define TIMER_CTRL_DIV32        (5 << 4)
#define TIMER_CTRL_DIV64        (6 << 4)
#define TIMER_CTRL_DIV128       (7 << 4)
#define TIMER_CTRL_PERIODIC     (0 << 7)
#define TIMER_CTRL_ONESHOT      (1 << 7)
#define TIMER_CTRL_CLK_OSC24M	(1 << 2)

#define TIMER_IRQ_EN_T0         (1 << 0)
#define TIMER_IRQ_EN_T1         (1 << 1)

#define TIMER_IRQ_ST_T0         (1 << 0)
#define TIMER_IRQ_ST_T1         (1 << 1)

#define TIMER_IRQ_CLR		(3)

struct spectimer {
	volatile uint32_t timercontrol;
	volatile uint32_t timerintvalue;
	volatile uint32_t timercurvalue;
	volatile uint32_t _padding;
};

typedef struct {
	volatile uint32_t timer_irqen;	/* 0x00 */
	volatile uint32_t timer_status;
	volatile uint32_t _padding[2];
	struct spectimer timers[2];
} sun4i_timer_t;

#endif /* SUN4I_TIMER_H */
