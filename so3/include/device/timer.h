
/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */


#ifndef TIME_H
#define TIME_H

#include <types.h>

#include <device/device.h>

#define HZ      1000

#define CLOCKSOURCE_MASK(bits) (u64)(bits<64 ? ((1ull<<bits)-1) : -1)

/* Time conversion units */

typedef unsigned int time_t;

struct timespec {
	long		tv_sec;			/* seconds */
	long		tv_nsec;		/* nanoseconds */
};

/* All timing information below must be express in nanoseconds. The underlying hardware is responsible
 * to perform the necessary alignment on 64 bits. */

/* Structure for a periodic timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	uint64_t period; /* Period in ns of the periodic timer */
	void (*start)(void);
} periodic_timer_t;

/* Structure for a oneshot timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	void (*set_delay)(uint64_t delay_ns);
	void (*start)(void);

	u32 mult;
	u32 shift;

	u64 max_delta_ns;
	u64 min_delta_ns;

} oneshot_timer_t;

/* Structure of a clocksource timer */
typedef struct {
	dev_t *dev; /* Associated hardware */
	u64 (*read)(void);

	uint32_t rate;

	u64 mask;
	u32 mult;
	u32 shift;

	/*
	 * Second part is written at each timer interrupt
	 * Keep it in a different cache line to dirty no
	 * more than one cache line.
	 */
	u64 cycle_last;

} clocksource_timer_t;

extern periodic_timer_t periodic_timer;
extern oneshot_timer_t oneshot_timer;
extern clocksource_timer_t clocksource_timer;

/**
 * cyc2ns - converts clocksource cycles to nanoseconds
 * @cs:		Pointer to clocksource
 * @cycles:	Cycles
 *
 * Uses the clocksource and ntp ajdustment to convert cycle_ts to nanoseconds.
 *
 * XXX - This could use some mult_lxl_ll() asm optimization
 */
static inline u64 cyc2ns(u64 cycles)
{
	return ((u64) cycles * clocksource_timer.mult) >> clocksource_timer.shift;
}

u64 get_s_time(void);

void timer_dev_init(void);
void timer_dev_set_deadline(u64 deadline);


#endif /* TIME_H */
