
/*
 *
 * -- Smart Object Oriented  --
 *
 * SO3 Operating System
 *
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 *
 * The contents of this file is strictly under the property of Sootech SA and must not be shared in any case.
 *
 * Contributors:
 *
 * - August 2017: Daniel Rossier
 *
 */

#ifndef SERIAL_H
#define SERIAL_H

/* Serial IOCTL  */
#define TIOCGWINSZ	0x5413

/*  This is a reserved char code we use to query (patched) Qemu to retrieve the window size. */
#define SERIAL_GWINSZ	"\254"

/* The following code is used for telling Qemu that SO3 is aborting its execution.
 * This is used for terminating qemu properly when codecheck is executed.
 * qemu-system-arm must be started with option --codecheck
 */
#define SERIAL_SO3_HALT	"\253"


typedef struct {
    int (*put_byte)(char c);
    char (*get_byte)(void);
} serial_ops_t;

extern serial_ops_t serial_ops;

struct winsize {
	unsigned short	ws_row;		/* rows, in characters */
	unsigned short	ws_col;		/* columns, in characters */
	unsigned short	ws_xpixel;	/* horizontal size, pixels */
	unsigned short	ws_ypixel;	/* vertical size, pixels */
};

int serial_putc(char c);
char serial_getc(void);

int serial_write(char *str, int max);
int serial_read(char *buf, int len);
int serial_gwinsize(struct winsize *wsz);

void serial_init(void);

extern void __ll_put_byte(char c);

#endif /* SERIAL_H */
