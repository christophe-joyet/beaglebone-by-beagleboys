/*
 * Automatically generated C config: don't edit
 * SO3 Configuration
 */
#define CONFIG_THREAD_ENV 1
#define CONFIG_APP_DEFAULT 1
#define CONFIG_STANDALONE 1
#define CONFIG_ITB 1
#define CONFIG_INT_C 1
#define CONFIG_RTC 1
#define CONFIG_WDT 1
#define CONFIG_TIMER 1
#define CONFIG_BEAGLEBONE 1
#define CONFIG_INTC 1
#define CONFIG_DEBUG_PRINTK 1
#define CONFIG_DM_TIMER 1
#define CONFIG_BIN 1
#define CONFIG_NS16550_UART 1
#define CONFIG_ELF 1
#define CONFIG_UART 1
#define CONFIG_RAM_BASE 0x80000000
#define CONFIG_KERNEL_VIRT_ADDR 0xb0000000
#define CONFIG_CROSS_COMPILE "arm-linux-gnueabihf-"
