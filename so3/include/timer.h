/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - April 2018: Daniel Rossier
 *
 * Timer management subsystem
 *
 */

#include <types.h>
#include <string.h>

#include <device/timer.h>

#define NSECS		1000000000ull


#define NOW()           ((u64) get_s_time())
#define SECONDS(_s)     ((u64)((_s)  * 1000000000ull))
#define MILLISECS(_ms)  ((u64)((_ms) * 1000000ull))
#define MICROSECS(_us)  ((u64)((_us) * 1000ull))
#define STIME_MAX 	((u64)(~0ull))

struct timer {
    /* System time expiry value (nanoseconds since boot). */
    u64 expires;

    /* Linked list. */
    struct timer *list_next;

    /* On expiry, '(*function)(data)' will be executed in softirq context. */
    void (*function)(void *);

    void *data;

    /* Timer status. */
#define TIMER_STATUS_inactive  0  /* Not in use; can be activated.    */
#define TIMER_STATUS_killed    1  /* Not in use; canot be activated.  */
#define TIMER_STATUS_in_list   2  /* In use; on overflow linked list. */

    uint8_t status;
};
typedef struct timer timer_t;

/*
 * All functions below can be called for any CPU from any CPU in any context.
 */

/*
 * Returns TRUE if the given timer is on a timer list.
 * The timer must *previously* have been initialised by init_timer(), or its
 * structure initialised to all-zeroes.
 */
static inline int active_timer(struct timer *timer)
{
    return (timer->status == TIMER_STATUS_in_list);
}

/*
 * Initialise a timer structure with an initial callback CPU, callback
 * function and callback data pointer. This function may be called at any
 * time (and multiple times) on an inactive timer. It must *never* execute
 * concurrently with any other operation on the same timer.
 */
static inline void init_timer(struct timer *timer, void (*function)(void *), void *data)
{
    memset(timer, 0, sizeof(*timer));

    timer->function = function;
    timer->data     = data;
}

/*
 * Set the expiry time and activate a timer. The timer must *previously* have
 * been initialised by init_timer() (so that callback details are known).
 */
extern void set_timer(struct timer *timer, u64 expires);

/*
 * Deactivate a timer This function has no effect if the timer is not currently
 * active.
 * The timer must *previously* have been initialised by init_timer(), or its
 * structure initialised to all zeroes.
 */
extern void stop_timer(struct timer *timer);

/*
 * Deactivate a timer and prevent it from being re-set (future calls to
 * set_timer will silently fail). When this function returns it is guaranteed
 * that the timer callback handler is not running on any CPU.
 * The timer must *previously* have been initialised by init_timer(), or its
 * structure initialised to all zeroes.
 */
extern void kill_timer(struct timer *timer);

/*
 * Bootstrap initialisation. Must be called before any other timer function.
 */
extern void timer_init(void);


/* Arch-defined function to reprogram timer hardware for new deadline. */
extern void reprogram_timer(u64 deadline);

void clocks_calc_mult_shift(u32 *mult, u32 *shift, u32 from, u32 to, u32 maxsec);

