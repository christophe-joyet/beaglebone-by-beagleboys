/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - August 2017: Danel Rossier
 *
 */

#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/serial.h>
#include <device/arch/pl011.h>

#include <mach/uart.h>

#include <asm/io.h>                 /* ioread/iowrite macros */

static dev_t pl011_dev =
{
  .base = UART_BASE,
};

static int  pl011_put_byte(char c) {

	while ((ioread16(pl011_dev.base + UART01x_FR) & UART01x_FR_TXFF)) ;

	iowrite16(pl011_dev.base + UART01x_DR, c);

	return 1;
}


static char pl011_get_byte(void) {

	/* Poll while nothing available */
	while (ioread8(pl011_dev.base + UART01x_FR) & UART01x_FR_RXFE);

	return ioread16(pl011_dev.base + UART01x_DR);
}

static int pl011_init(dev_t *dev) {

	/* Init pl011 UART */

	memcpy(&pl011_dev, dev, sizeof(dev_t));

	serial_ops.put_byte = pl011_put_byte;
	serial_ops.get_byte = pl011_get_byte;


	return 0;
}

void __ll_put_byte(char c) {

	pl011_put_byte(c);

}


REGISTER_DRIVER(pl011_uart, "serial,pl011", pl011_init);
