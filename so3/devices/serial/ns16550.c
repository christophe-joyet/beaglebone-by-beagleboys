#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/serial.h>
#include <device/arch/ns16550.h>

#include <mach/uart.h>

#include <asm/io.h>                 /* ioread/iowrite macros */


static dev_t ns16550_dev =
{
  .base = UART_BASE,
};

static struct ns16550_t* ns16550_uart;


static int ns16550_put_byte(char c) {

#if 0
	if(ns16550_dev.base  == 0x44e09000){
		int* add;

		add = 0x4804C000 + 0x134;

		*add = 0xffdfffff;

		add = 0x4804C000 + 0x194;

		*add = 0x00200000;

	}
#endif

	if(c == '\n')
		ns16550_put_byte('\r');

	/* Poll while nothing available */
	while ((ioread8(ns16550_dev.base + UART_THR) & UART_LSR_THRE) == 0);

	return iowrite16(ns16550_dev.base + UART_THR/*+ UART_LSR*/, c);
}


static char ns16550_get_byte(void) {

	/* Poll while nothing available */
	while ((ioread8(ns16550_dev.base + UART_LSR) & UART_LSR_DR) == 0);

	return ioread16(ns16550_dev.base + UART_LSR/*+ UART_THR*/);
}

static int ns16550_init(dev_t *dev) {


	/* Init pl011 UART */
	memcpy(&ns16550_dev, dev, sizeof(dev_t));

	ns16550_uart  = (struct ns16550_t*)(ns16550_dev.base);

	while (!(ioread8(ns16550_dev.base + UART_LSR) & UART_LSR_TEMT));
	iowrite8(ns16550_dev.base + UART_IER, 0x0);
	//iowrite8(ns16550_dev.base + UART_MDR1, 0x7);
	//iowrite8(ns16550_dev.base + UART_LCR, 0x83);
	iowrite8(ns16550_dev.base + UART_DLL, UART_BAUDRATE & 0xff);
	iowrite8(ns16550_dev.base + UART_IER, UART_BAUDRATE>>8);
	//iowrite8(ns16550_dev.base + UART_LCR, 0x3);


	serial_ops.put_byte = ns16550_put_byte;
	serial_ops.get_byte = ns16550_get_byte;


	return 0;
}

void __ll_put_byte(char c) {

	ns16550_put_byte(c);

}


REGISTER_DRIVER(ns16550_uart, "serial,ns16550", ns16550_init);
