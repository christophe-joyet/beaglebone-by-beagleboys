/*
 * intc.c
 *
 *  Created on: Nov 21, 2018
 *      Author: Joyet & Labie
 */

#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/irq.h>
#include <device/arch/intc.h>


#include <asm/io.h>                 /* ioread/iowrite macros */



static dev_t intc_dev;
static struct intc_regs* intc_regs;


// Activation d'une interruption
static void intc_irq_enable(unsigned int irq){

	unsigned int register_nbr;
	unsigned int bit_nbr;

	if(irq >= NR_IRQS){
		printk("IRQ number bigger than the actual amount of IRQs\n");
		return;
	}

	register_nbr = irq / 32;
	bit_nbr 	 = irq % 32;

	// is signifie Interrupt Set. Donc on va activé une IRQ
	// en écrivant dans son bit correspondant dans un registre.
	switch(register_nbr){
	case REG_IRQ0:
		intc_regs->intc_mir_clear0 |= (1 << bit_nbr);
		break;
	case REG_IRQ1:
		intc_regs->intc_mir_clear1 |= (1 << bit_nbr);
		break;
	case REG_IRQ2:
		intc_regs->intc_mir_clear2 |= (1 << bit_nbr);
		break;
	case REG_IRQ3:
		intc_regs->intc_mir_clear3 |= (1 << bit_nbr);
		break;
	}
}

// Désactivation d'une interruption
//static void intc_irq_disable(unsigned int irq){
void intc_irq_disable(unsigned int irq){

	unsigned int register_nbr;
	unsigned int bit_nbr;

	if(irq >= NR_IRQS){
		printk("IRQ number bigger than the actual amount of IRQs\n");
		return;
	}

	register_nbr = irq / 32;
	bit_nbr 	 = irq % 32;

	// ic signifie Interrupt Clear. Donc on va désactivé une IRQ
	// en écrivant dans son bit correspondant dans un registre.
	switch(register_nbr){
		case REG_IRQ0:
			intc_regs->intc_mir_set0 |= (1 << bit_nbr);
			break;
		case REG_IRQ1:
			intc_regs->intc_mir_set1 |= (1 << bit_nbr);
			break;
		case REG_IRQ2:
			intc_regs->intc_mir_set2 |= (1 << bit_nbr);
			break;
		case REG_IRQ3:
			intc_regs->intc_mir_set3 |= (1 << bit_nbr);
			break;
	}
}

// Masquage d'une interruption, donc il faut la desactivée
static void intc_irq_mask(unsigned int irq){
	intc_irq_disable(irq);
}

// Démasquage d'une interruption, donc il faut l'activée.
static void intc_irq_unmask(unsigned int irq){
	intc_irq_enable(irq);
}

// Traitement lors de l'interception d'une interruption.
static void intc_irq_handle(void){

	u32 irq;

	do{

		// On cherche quelle est le numéro de l'IRQ généré.
		// On le filtre pour s'assurer qu'il n'y ait pas de flag
		// d'une spurious IRQ.
		irq = intc_regs->intc_sir_irq & 0x7f;

		// Si la valeur restante est 0, alors l'IRQ générée
		// était spurious.
		if(!irq){
			printk("IRQ not handled.\n");
			return;
		}

		// procède à la routine d'interruption associée à l'IRQ.
		irq_process(irq);

		// On reset la génération d'IRQ.
		intc_regs->intc_ctrl |= NEWIRQ_RESET << NEWIRQAGR;

		// Boucle tans qu'une IRQ est en attente.
	}while(intc_regs->intc_pending_irq0 |
		   intc_regs->intc_pending_irq1 |
		   intc_regs->intc_pending_irq2 |
		   intc_regs->intc_pending_irq3);
}

static int intc_init(dev_t *dev) {

	memcpy(&intc_dev, dev, sizeof(dev_t));

	intc_regs = (struct intc_regs*)(intc_dev.base);


	// On effectue un soft reset du module. Cette action aura pour effet de remettre
	// tous les registres du controlleur d'interruption à leur valeur par défaut. Ces
	// valeurs sont intéressante dans notre cas.
	intc_regs->intc_sysconfig |= SOFTRESET;
	while(!(intc_regs->intc_systatus[0] & 0x1)); // on attend la fin du soft reset.


	// Attribution des fonctions.
	irq_ops.irq_enable 	= intc_irq_enable;
	irq_ops.irq_disable = intc_irq_disable;
	irq_ops.irq_mask 	= intc_irq_mask;
	irq_ops.irq_unmask 	= intc_irq_unmask;
	irq_ops.irq_handle 	= intc_irq_handle;

	intc_regs->intc_ctrl |= NEWIRQ_RESET << NEWIRQAGR;

	return 0;
}


REGISTER_DRIVER(intc_regs, "intc,gic", intc_init);		// Macro qui ajoute les drivers avec un script de linkage

