/*
 * gic.c
 *
 *  Created on: Nov 21, 2018
 *      Author: Joyet & Labie
 */

#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/irq.h>
#include <device/arch/gic.h>

#include <mach/timer.h>

#include <asm/io.h>                 /* ioread/iowrite macros */



static dev_t gic_dev;
static struct intc_regs* gic_regs;


// Activation d'une interruption
static void gic_irq_enable(unsigned int irq){

	unsigned int register_nbr;
	unsigned int bit_nbr;

	if(irq >= NR_IRQS){
		printk("IRQ number bigger than the actual amount of IRQs\n");
		return;
	}

	register_nbr = irq / 32;
	bit_nbr 	 = irq % 32;

	// is signifie Interrupt Set. Donc on va activé une IRQ
	// en écrivant dans son bit correspondant dans un registre.
	gic_regs->gicd_isenabler[register_nbr]   |= (1 << bit_nbr);
}



// Désactivation d'une interruption
static void gic_irq_disable(unsigned int irq){

	unsigned int register_nbr;
	unsigned int bit_nbr;

	if(irq >= NR_IRQS){
		printk("IRQ number bigger than the actual amount of IRQs\n");
		return;
	}

	register_nbr = irq / 32;
	bit_nbr 	 = irq % 32;

	// ic signifie Interrupt Clear. Donc on va désactivé une IRQ
	// en écrivant dans son bit correspondant dans un registre.
	gic_regs->gicd_icenabler[register_nbr]  |= (1 << bit_nbr);
}

// Masquage d'une interruption, donc il faut la desactivée
static void gic_irq_mask(unsigned int irq){
	gic_irq_disable(irq);
}


// Démasquage d'une interruption, donc il faut l'activée.
static void gic_irq_unmask(unsigned int irq){
	gic_irq_enable(irq);
}


// Traitement lors de l'interception d'une interruption.
static void gic_irq_handle(void){
	int i;


	// Le registre gicc_iar nous indique quel interruption a été levée.
	// La valeur qu'il contient sur ses bits 9...0 est le numéro de l'interruption.
	// Avec un masque 0x3ff, on peut obtenir uniquement cette valeur.
	// Les bits 12...10 eux indique le CPU ayant généré une SGI, donc un masque
	// 0x1c00.
	// Attention, lire dans le registre effectue une action clear.
	unsigned int iar    = gic_regs->gicc_iar & 0x1fff;
	unsigned int irq 	= iar & 0x3ff;
	unsigned int cpuID  = iar & 0x1c00;


	// La valeur par défaut de l'iar est 1023. lorsqu'une interruption arrive, elle
	// est modifiée par le numéro de l'interruption, pour autant que :
	// - la distribution de l'irq à l'interface cpu est désactivée
	// - la distribution de l'interfacce au cpu désiré desactivée
	// - la priorité de l'IRQ n'est pas suffisante pour être signalée au CPU.
	// Dans tous ces cas, on ne souhaite pas traité d'interruption.
	if(irq == GICC_INT_SPURIOUS){
		printk("IRQ not handled.\n");
		return;
	}


	irq_process(irq);

	// On récris dans le gicc_eoir pour informer que l'on a terminé
	// de traiter l'interruption.
	gic_regs->gicc_eoir |= iar;
	gic_regs->gicd_icpendr[irq/32] |= irq%32;
}


static int gic_init(dev_t *dev) {


	int i;

	memcpy(&gic_dev, dev, sizeof(dev_t));

	gic_regs = (struct intc_regs*)(gic_dev.base);


	/* Initialisation de la GIC */

	// La GIC est composée deux "2" interface :
	// La GICD, représentant l'interface qui redistribue les IRQ perçu
	// au cpu
	// La GICC, représentant l'interface qui reçoit les interruptions de la GICD.

	// Désactivation de la distribution des IRQ à l'interface CPU le temps de l'initialisation
	gic_regs->gicd_ctlr &= ~GICC_ENABLE;

	// On indique à la GIC que les IRQ de type SPI sont détecté de manière asynchrone
	// 1 registre traite 16 IRQ. On veut aller de 32 à 122 (on peut aller jusqu'à 127). Il y a 128 IRQ.
	for (i = 32; i < NR_IRQS; i += 16){
		gic_regs->gicd_icfgr[i/16]  &= 0x0;
	}

	// On définis quels seront les processeurs cibles de nos IRQ.
	// Pour chaque interruptions, on la relie au processeur 1, le seul qu'on a (d'après le device-tree).
	// 1 registre traite 4 IRQ. On veut aller de 32 à 122 (on peut aller jusqu'à 127). Il y a 128 IRQ.
	for (i = 32; i < NR_IRQS; i += 4){
		gic_regs->gicd_itargetsr[i/4] 	= (0x1 << 24 ) | (0x1 << 16) | (0x1 << 8) | (0x1 << 0);
	}


	// Configuration de la prioirté de chaque interruption SPI.
	// 1 registre traite 4 IRQ. On veut aller de 32 à 122 (on peut aller jusqu'à 127). Il y a 128 IRQ.
	for (i = 32; i < NR_IRQS; i += 4){
		gic_regs->gicd_ipriorityr[i/4] = (GIC_CPU_HIGHPRI << 24) | (GIC_CPU_HIGHPRI << 16) | (GIC_CPU_HIGHPRI << 8) | (GIC_CPU_HIGHPRI);
	}


	// Clear enable registers.
	// En écrivant 1, on désactive le forwarding des interrutpions. Elles seront activée 1 à 1 avec la
	// fonction gic_irq_enble.
	// 1 registre traite 32 IRQ. On veut aller de 32 à 122 (on peut aller jusqu'à 127). Il y a 128 IRQ.
	for (i = 32; i < NR_IRQS; i += 32){
		gic_regs->gicd_icenabler[i/32] |= 0xffffffff;
	}


	// Activation de la distribution
	gic_regs->gicd_ctlr = 0x1;

	// Desactivation de toutes les interruptions de type software ou
	// de périphérique privé (SGI et PPI).
	gic_regs->gicd_icenabler[0] |= 0xffffffff;

	// Configuration de la prioirté de chaque interruption SGI et PPI
	// 1 registre traite 4 IRQ. On veut aller de 32 à 122 (on peut aller jusqu'à 127). Il y a 128 IRQ.
	for (i = 0; i < 32; i += 4){
		gic_regs->gicd_ipriorityr[i/4] = (GIC_CPU_HIGHPRI << 24) | (GIC_CPU_HIGHPRI << 16) | (GIC_CPU_HIGHPRI << 8) | (GIC_CPU_HIGHPRI);
	}

	// On détermine le niveau de priorité maximum
	// que le cpu acceptera de traité.
	gic_regs->gicc_pmr |=  GICC_INT_PRI_THRESHOLD;

	// On active la signalisation d'IRQ par la gicD au CPU
	gic_regs->gicc_ctlr |=  GICC_ENABLE;


	// Attribution des fonctions.
	irq_ops.irq_enable 	= gic_irq_enable;
	irq_ops.irq_disable = gic_irq_disable;
	irq_ops.irq_mask 	= gic_irq_mask;
	irq_ops.irq_unmask 	= gic_irq_unmask;
	irq_ops.irq_handle 	= gic_irq_handle;

	return 0;
}


REGISTER_DRIVER(gic_regs, "intc,gic", gic_init);		// Macro qui ajoute les drivers avec un script de linkage

