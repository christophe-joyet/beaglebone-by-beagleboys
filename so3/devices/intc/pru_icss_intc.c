/*
 * intc.c
 *
 *  Created on: Nov 21, 2018
 *      Author: Joyet & Labie
 */

#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/irq.h>
#include <device/arch/pru_icss_intc.h>

#include <mach/timer.h>

#include <asm/io.h>                 /* ioread/iowrite macros */

static dev_t pru_icss_intc_dev;

static int pru_icss_intc_init(dev_t *dev) {

	memcpy(&pru_icss_intc_dev, dev, sizeof(dev_t));

	// set polarity
	*(int*)(pru_icss_intc_dev.base + SPIR0) = POLARITY_31_0_HIGH;
	*(int*)(pru_icss_intc_dev.base + SPIR1) = POLARITY_63_32_HIGH;
	// set type to pulse
	*(int*)(pru_icss_intc_dev.base + SITR0) = SET_TYPE_PULSE;
	*(int*)(pru_icss_intc_dev.base + SITR1) = SET_TYPE_PULSE;
	/* Clear system event by writing 1s to SECR registers */
	*(int*)(pru_icss_intc_dev.base + SECR0) = CLEAR_SECR;
	*(int*)(pru_icss_intc_dev.base + SECR1) = CLEAR_SECR;
	/* Enable host interrupt by writing index value to HIER register. */
	*(int*)(pru_icss_intc_dev.base + HIER)	= EN_HINT;
	/* Globally enable all interrupts through GER register */
	*(int*)(pru_icss_intc_dev.base + GER)	= EN_HINT_ANY;


	return 0;
}


REGISTER_DRIVER(pru_icss_intc, "pru_icss_intc", pru_icss_intc_init);		// Macro qui ajoute les drivers avec un script de linkage

