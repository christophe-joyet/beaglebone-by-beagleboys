/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - 2017-2018: Daniel Rossier
 *
 */

#include <spinlock.h>
#include <timer.h>
#include <softirq.h>

#include <device/timer.h>
#include <device/irq.h>

static u64 sys_time = 0ull;

/* The three main timers in SO3 */

periodic_timer_t periodic_timer;
oneshot_timer_t oneshot_timer;
clocksource_timer_t clocksource_timer;

/*
 * Return the time in ns from the monotonic clocksource.
 * May be called simultaneously by the agency CPU  and the ME CPU
 */
u64 get_s_time(void) {
	u64 cycle_now, cycle_delta;
	uint32_t flags;

	/* Protect against concurrent access from different CPUs */

	flags = local_irq_save();

	if (clocksource_timer.read != NULL)
		cycle_now = clocksource_timer.read();
	else
		return 0;

	cycle_delta = (cycle_now - clocksource_timer.cycle_last) & clocksource_timer.mask;

	clocksource_timer.cycle_last = cycle_now;

	sys_time += cyc2ns(cycle_delta);

	local_irq_restore(flags);

	return sys_time;
}

/*
 * repogram_timer - May be used from various CPUs
 *
 * deadline is the expiring time expressed in ns.
 *
 */
void timer_dev_set_deadline(u64 deadline) {
	int64_t delta;

	/* Only the oneshot timer will be possibly reprogrammed */

	delta = deadline - NOW();

	if (delta <= 0)
		raise_softirq(TIMER_SOFTIRQ);
	else {
		if (oneshot_timer.dev == NULL)
			return ;

		oneshot_timer.set_delay(delta);
	}
}


void timer_dev_init(void) {

	memset(&periodic_timer, 0, sizeof(periodic_timer_t));
	memset(&oneshot_timer, 0, sizeof(oneshot_timer_t));
	memset(&clocksource_timer, 0, sizeof(clocksource_timer_t));

}

