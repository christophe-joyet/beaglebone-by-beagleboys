cmd_devices/device.o := arm-linux-gnueabihf-gcc -Wp,-MD,devices/.device.o.d  -Iinclude  -I.  -include include/generated/autoconf.h -D__KERNEL__ -D__SO3__ -Iinclude -fno-builtin -ffreestanding -nostdinc -isystem /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include -g -O0 -fno-common -marm -mno-thumb-interwork -march=armv7-a -Wall -Wstrict-prototypes -D__KERNEL__ -D__SO3__ -Iinclude -fno-builtin -ffreestanding -nostdinc -isystem /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include -I. -I./lib/libfdt  -Iarch/arm/include/ -Iarch/arm/beaglebone/include/	  -c -o devices/device.o devices/device.c

source_devices/device.o := devices/device.c

deps_devices/device.o := \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/so3virt.h) \
    $(wildcard include/config/rootfs/ramdev.h) \
  include/common.h \
  include/types.h \
  arch/arm/include/asm/types.h \
  include/linker.h \
  include/printk.h \
  /opt/toolchains/gcc-linaro-arm-linux-gnueabihf-4.7-2013.04-20130415_linux/bin/../lib/gcc/arm-linux-gnueabihf/4.7.3/include/stdarg.h \
  include/heap.h \
  include/sizes.h \
  include/memory.h \
    $(wildcard include/config/kernel/virt/addr.h) \
    $(wildcard include/config/ram/base.h) \
  include/list.h \
  include/prefetch.h \
  arch/arm/include/asm/processor.h \
  include/compiler.h \
  arch/arm/include/asm/memory.h \
  arch/arm/include/asm/setup.h \
  include/device/device.h \
  include/device/fdt/fdt.h \
  include/device/fdt/libfdt_env.h \
  include/string.h \
  include/device/driver.h \
  include/device/serial.h \
  include/device/irq.h \
  include/thread.h \
  include/schedule.h \
  include/device/timer.h \
  include/device/ramdev.h \

devices/device.o: $(deps_devices/device.o)

$(deps_devices/device.o):
