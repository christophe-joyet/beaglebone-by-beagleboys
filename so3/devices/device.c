/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 *
 * This software is released under the MIT License whose terms are defined hereafter.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - October 2017: Daniel Rossier
 * - January 2018: Daniel Rossier
 *
 */


#if 0
#define DEBUG
#endif

#include <common.h>
#include <heap.h>
#include <memory.h>

#include <asm/setup.h>

#include <device/device.h>
#include <device/driver.h>
#include <device/serial.h>
#include <device/irq.h>
#include <device/timer.h>
#include <device/ramdev.h>

/*
 * Device status strings
 */
static char *__dev_state_str[] = {
		"unknown",
		"disabled",
		"init pending",
		"initialized",
};

char *dev_state_str(dev_status_t status) {
	return __dev_state_str[status];
}

/*
 * Read the content of a device tree and associate a generic device info structure to each
 * relevant entry.
 *
 * So far, the device tree can have only one level of subnode (meaning that the root can contain only
 * nodes at the same level. Managing further sub-node levels require to adapt kernel/fdt.c
 *
 */

void parse_dtb(void) {
	unsigned int drivers_count;
	driver_entry_t *driver_entries;
	dev_t *dev;
	int i;
	int offset, new_off;
	bool found;

	drivers_count = ll_entry_count(driver_entry_t, driver);
	driver_entries = ll_entry_start(driver_entry_t, driver);

	offset = 0;

	DBG("Now scanning the device tree to retrieve all devices...\n");

	dev = (dev_t *) malloc(sizeof(dev_t));
	ASSERT(dev != NULL);
	found = false;

	while ((new_off = get_dev_info((void *) _fdt_addr, offset, "*", dev)) != -1) {

		for (i = 0; i < drivers_count; i++) {

			if (strcmp(dev->compatible, driver_entries[i].compatible) == 0) {

				found = true;

				DBG("Found compatible: %s\n",driver_entries[i].compatible);
				DBG("    Compatible:   %s\n", dev->compatible);
				DBG("    Base address: 0x%08X\n", dev->base);
				DBG("    Size:         0x%08X\n", dev->size);
				DBG("    IRQ:          %d\n", dev->irq);
				DBG("    Status:       %s\n", dev_state_str(dev->status));

				if (dev->status != STATUS_DISABLED) {
#ifdef CONFIG_MMU
					/* Perform the mapping in the I/O virtual address space, if necessary (size > 0) */
					if (dev->size > 0) {
						dev->base = io_map(dev->base, dev->size);
						DBG("    Virtual addr: 0x%x\n", dev->base);
					}
#endif
					driver_entries[i].init(dev);
					dev->status = STATUS_INITIALIZED;

				}
				break;
			}
		}

		if (!found)
			free(dev);

		offset = new_off;

		dev = (dev_t *) malloc(sizeof(dev_t));
		ASSERT(dev != NULL);
		found = false;
	}

	/* We have always the last allocation which will not be used */
	free(dev);
}

/*
 * Main device initialization function.
 */
void devices_init(void) {

#ifdef CONFIG_SO3VIRT
	board_setup_post();
#endif
	/* Interrupt management subsystem initialization */
	irq_init();

	serial_init();


	timer_dev_init();

#ifdef CONFIG_ROOTFS_RAMDEV
	/* Get possible ram device (aka initrd loaded from U-boot) */
	ramdev_init();
#endif

	/* Pare the associated dtb to initialize all devices */
	parse_dtb();
}
