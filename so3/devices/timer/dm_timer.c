/*
 * dm_timer.c
 *
 *  Created on: Oct 17, 2018
 *      Author: Joyet & Labie
 */


#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/timer.h>
#include <device/arch/dm_timer.h>
#include <device/arch/watchdog_timer.h>


#include <asm/io.h>                 /* ioread/iowrite macros */
#include <device/irq.h>				// Pour traitement d'interruption
#include <calibrate.h>				// pour callibration




static dev_t dm_periodic_timer_dev;
static dev_t dm_oneshot_timer_dev;
static dev_t cm_dpll_dev;


static struct dm_timer* dm_periodic_timer;
static struct dm_timer* dm_oneshot_timer;
static struct cm_dpll* cm_dpll;



//===================================================================//
//========== 		 	Fonction One Shot Timer	   		   ==========//
//===================================================================//


static void dm_oneshot_timer_start(void){
	//printk("START ONE SHOT!\n");
	dm_oneshot_timer->tclr	|= TCLR_ST;
}


static void dm_oneshot_timer_set_delay(uint64_t delay_ns){
	dm_oneshot_timer->tldr = TIMER_OVERFLOW - (((uint64_t)CLK_32KHZ) * delay_ns)/1000000000;
	dm_oneshot_timer->ttgr = 0x1;
}


static irq_return_t dm_oneshot_timer_irq_handler(int irq, void *data){
	irq_return_t irq_ret;

	dm_oneshot_timer->irqenable_clear = OVF_EN_FLAG;

	//printk("Oneshot Timer IRQ !\n");

	// On nettoye l'interruption de notre timer.
	dm_oneshot_timer->irqstatus = OVF_EN_FLAG;

	// On doit retourner IRQ_BOTTOM pour que l'action différée soit effectuée à son tour.
	irq_ret = IRQ_COMPLETED;

	//dm_oneshot_timer->tier = OVF_EN_FLAG;
	dm_oneshot_timer->irqenable_set = OVF_EN_FLAG;

	return irq_ret;
}

static int dm_oneshot_timer_init(dev_t *dev) {

	memcpy(&dm_oneshot_timer_dev, dev, sizeof(dev_t));
	dm_oneshot_timer = (struct dm_timer*)(dm_oneshot_timer_dev.base);

	// Soft reset du module. Remet tous les registres du timer à leur valeur par défaut
	dm_oneshot_timer->tiocp_cfg[0] |= 0x1;
	while(dm_oneshot_timer->tiocp_cfg[0] & 0x1);


	// On set le timer en mode oneshot.
	dm_oneshot_timer->tclr &= ~TCLR_AR;


	// Activation des interruption de type overflow
	dm_oneshot_timer->irqenable_set 	|= OVF_EN_FLAG;


	// On associe notre device, la fonction set_delay, ainsi que la fonction start
	// à la partie générique du timer SO3.
	oneshot_timer.dev 		= &dm_oneshot_timer_dev;
	oneshot_timer.set_delay = dm_oneshot_timer_set_delay;
	oneshot_timer.start 	= dm_oneshot_timer_start;

	// On associe notre fonction dm_oneshot_timer_dev à l'interruption
	// dm_oneshot_timer_dev.irq. Ainsi, lorsqu'elle sera levée par le cpu, celui-ci
	// appelera cette fonction pour effectuer une tâche.
	irq_bind(dm_oneshot_timer_dev.irq, &dm_oneshot_timer_irq_handler, NULL, NULL);

	return 0;
}


//===================================================================//
//========== 		 	Fonction Periodic Timer	   		   ==========//
//===================================================================//

static void dm_periodic_timer_start(void){
	dm_periodic_timer->tclr	|= TCLR_ST;
}

static irq_return_t dm_periodic_timer_irq_handler(int irq, void *data){
//static irq_return_t dm_periodic_timer_irq_handler(int irq, void *data){
	irq_return_t irq_ret;

	static unsigned int i = 0;

	dm_periodic_timer->irqenable_clear = OVF_EN_FLAG;

	jiffies++;

	//printk("Periodic Timer IRQ nbr : %d\n", i++);

	// On nettoye l'interruption de notre timer.
	dm_periodic_timer->irqstatus = OVF_EN_FLAG;

	// On doit retourner IRQ_BOTTOM pour que l'action différée soit effectuée à son tour.
	irq_ret = IRQ_COMPLETED;


	// Chaque IRQ, on initialise un timer one_shot.
	if(i%5 == 0){
		dm_oneshot_timer_set_delay(2000000000);
		dm_oneshot_timer_start();
	}

#ifdef CONFIG_WDT
	reset_watchdog();
#endif


	dm_periodic_timer->irqenable_set = OVF_EN_FLAG;

	return irq_ret;
}

static int dm_periodic_timer_init(dev_t *dev) {

	memcpy(&dm_periodic_timer_dev, dev, sizeof(dev_t));

	dm_periodic_timer = (struct dm_timer*)(dm_periodic_timer_dev.base);

	// Soft reset du module. Remet tous les registres du timer à leur valeur par défaut
	dm_periodic_timer->tiocp_cfg[0] |= 0x1;
	while(dm_periodic_timer->tiocp_cfg[0] & 0x1);

	// On set le timer en mode périodique.
	dm_periodic_timer->tclr |= TCLR_AR;


	// On lui set une période de 1 secondes.
	dm_periodic_timer->tldr = TIMER_OVERFLOW - CLK_32KHZ;
	dm_periodic_timer->ttgr	= 0x1;		//writing a random value int the ttgr reg so that the tcrr load the tldr value.


	// Activation des interruption de type overflow
	dm_periodic_timer->irqenable_set 	|= OVF_EN_FLAG;




	// On associe notre device, la période de notre timer, ainsi que la fonction start
	// à la partie générique du timer SO3.
	periodic_timer.dev 		= &dm_periodic_timer_dev;
	//periodic_timer.period 	= CLK_32KHZ;
	periodic_timer.start 	= dm_periodic_timer_start;

	// On associe notre fonction dm_periodic_timer_dev à l'interruption
	// dm_periodic_timer_dev.irq. Ainsi, lorsqu'elle sera levée par le cpu, celui-ci
	// appelera cette fonction pour effectuer une tâche.
	irq_bind(dm_periodic_timer_dev.irq, &dm_periodic_timer_irq_handler, NULL, NULL);

	return 0;
}

static int clock_module_pll_init(dev_t *dev) {

	memcpy(&cm_dpll_dev, dev, sizeof(dev_t));
	cm_dpll = (struct cm_dpll*)(cm_dpll_dev.base);

	// On set la clock source du dmtimer2 comme étant la clock 32kHz.
	cm_dpll->clksel_timer2_cl = 0x2;

	return 0;
}


REGISTER_DRIVER(dm_periodic_timer, "dm,periodic-timer", dm_periodic_timer_init);		// Macro qui ajoute les drivers avec un script de linkage
REGISTER_DRIVER(dm_oneshot_timer, "dm,oneshot-timer", dm_oneshot_timer_init);
REGISTER_DRIVER(clock_module_pll, "clock-module-dpll", clock_module_pll_init);


