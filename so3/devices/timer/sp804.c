/*
 * sp804.c
 *
 *  Created on: Oct 17, 2018
 *      Author: Joyet & Labie
 */



#include <types.h>
#include <heap.h>

#include <device/device.h>
#include <device/driver.h>

#include <device/timer.h>
#include <device/arch/sp804.h>

#include <mach/timer.h>

#include <asm/io.h>                 /* ioread/iowrite macros */
#include <device/irq.h>				// Pour traitement d'interruption
#include <calibrate.h>				// pour callibration




static dev_t sp804_periodic_timer_dev;
static dev_t sp804_oneshot_timer_dev;


static struct sp804_timer* sp804_periodic_timer;
static struct sp804_timer* sp804_oneshot_timer;



//===================================================================//
//========== 		 	Fonction Oneshot Timer	   		   ==========//
//===================================================================//

static void sp804_oneshot_timer_start(void){
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_ENABLE;
}



static void sp804_oneshot_timer_set_delay(uint64_t delay_ns){
	sp804_oneshot_timer->timerload		= (((uint64_t)TIMER_RATE) * delay_ns)/1000000000;
}



static irq_return_t sp804_oneshot_timer_irq_handler(int irq, void *data){
	irq_return_t irq_ret;

	printk("Oneshot Timer IRQ !\n");

	// On nettoye l'interruption de notre timer.
	sp804_oneshot_timer->timerintclr = TIMER_INTCLR;

	// On indique que le traitement de l'interruption est terminé
	irq_ret = IRQ_COMPLETED;

	return irq_ret;
}



static int sp804_oneshot_timer_init(dev_t *dev) {

	memcpy(&sp804_oneshot_timer_dev, dev, sizeof(dev_t));

	sp804_oneshot_timer  = (struct sp804_timer*)(sp804_oneshot_timer_dev.base);

	// Initialisation des registres :
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_ONESHOT;
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_32BIT;
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_DIV1;
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_IE;
	sp804_oneshot_timer->timercontrol |= TIMER_CTRL_PERIODIC;



	// On associe notre device, la fonction setDelay, ainsi que la fonction start
	// à la partie générique du oneshot timer SO3.
	oneshot_timer.dev 			= &sp804_oneshot_timer_dev;
	oneshot_timer.set_delay 	= sp804_oneshot_timer_set_delay;
	oneshot_timer.start 		= sp804_oneshot_timer_start;
	//oneshot_timer.mult 			= ;									// u32
	//oneshot_timer.shift 			= ;									// u32
	//oneshot_timer.max_delta_ns 	= ;									// u64
	//oneshot_timer.min_delta_ns 	= ;									// u64


	// On associe notre fonction sp804_oneshot_timer_irq_handler à l'interruption
	// sp804_oneshot_timer_dev.irq. Ainsi, lorsqu'elle sera levée par le cpu, celui-ci
	// appelera cette fonction pour effectuer une tâche.
	irq_bind(sp804_oneshot_timer_dev.irq, &sp804_oneshot_timer_irq_handler, NULL, NULL);

	return 0;
}



//===================================================================//
//========== 		 	Fonction Periodic Timer	   		   ==========//
//===================================================================//


static void sp804_periodic_timer_start(void){
	sp804_periodic_timer->timercontrol |= TIMER_CTRL_ENABLE;
}


static irq_return_t sp804_periodic_timer_irq_handler(int irq, void *data){
	irq_return_t irq_ret;

	static unsigned int i = 0;

	printk("Periodic Timer IRQ nbr : %d\n", i++);

	if((i%5) == 0){
		sp804_oneshot_timer_set_delay(10000);
		sp804_oneshot_timer_start();
	}

	// On nettoye l'interruption de notre timer.
	sp804_periodic_timer->timerintclr = TIMER_INTCLR;

	// On doit retourner IRQ_BOTTOM pour que l'action différée soit effectuée à son tour.
	irq_ret = IRQ_COMPLETED;

	return irq_ret;
}


/*static irq_return_t sp804_periodic_timer_irq_differed(int irq, void *data){
	irq_return_t irq_ret;

	static unsigned int irq_counter = 0;

	printk("\n\n\n\n\n\n\n\n### Timer IRQ nbr : %d\n\n\n\n\n\n\n\n\n\n\n", irq_counter++);

	//calibrate_delay();

	// On indique que le traitement de l'interruption est terminé
	irq_ret = IRQ_COMPLETED;

	return irq_ret;
}*/


static int sp804_periodic_timer_init(dev_t *dev) {

	memcpy(&sp804_periodic_timer_dev, dev, sizeof(dev_t));

	sp804_periodic_timer = (struct sp804_timer*)(sp804_periodic_timer_dev.base);


	// Initialisation des registres :

	sp804_periodic_timer->timercontrol &= ~TIMER_CTRL_ONESHOT;
	sp804_periodic_timer->timercontrol |= TIMER_CTRL_32BIT;
	sp804_periodic_timer->timercontrol |= TIMER_CTRL_DIV1;
	sp804_periodic_timer->timercontrol |= TIMER_CTRL_IE;
	sp804_periodic_timer->timercontrol |= TIMER_CTRL_PERIODIC;

	// On lui set une période de 1 secondes.
	sp804_periodic_timer->timerload		= TIMER_RATE;



	// On associe notre device, la période de notre timer, ainsi que la fonction start
	// à la partie générique du timer SO3.
	periodic_timer.dev 		= &sp804_periodic_timer_dev;
	periodic_timer.period 	= TIMER_RATE;
	periodic_timer.start 	= sp804_periodic_timer_start;


	// On associe notre fonction sp804_periodic_timer_dev à l'interruption
	// sp804_periodic_timer_dev.irq. Ainsi, lorsqu'elle sera levée par le cpu, celui-ci
	// appelera cette fonction pour effectuer une tâche.
	irq_bind(sp804_periodic_timer_dev.irq, &sp804_periodic_timer_irq_handler, NULL, NULL);
	// Deplus, on lui ajoute comme traitement différé la fonction sp804_periodic_timer_irq_differed, qui
	// appelera la fonction calibrate, comme demandé dans la consigne.
	// Mais quelque chose ne marche pas avec
	//irq_bind(sp804_periodic_timer_dev.irq, &sp804_periodic_timer_irq_handler, &sp804_periodic_timer_irq_differed, NULL);

	return 0;
}




REGISTER_DRIVER(sp804_periodic_timer, "sp804,periodic-timer", sp804_periodic_timer_init);		// Macro qui ajoute les drivers avec un script de linkage
REGISTER_DRIVER(sp804_oneshot_timer, "sp804,oneshot-timer", sp804_oneshot_timer_init);


