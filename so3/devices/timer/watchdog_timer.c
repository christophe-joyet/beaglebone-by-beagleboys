/*
 * watchdog_timer.c
 *
 *  Created on: Jan 23, 2019
 *      Author: reds
 */


#include <types.h>
#include <heap.h>
#include <device/arch/watchdog_timer.h>
#include <device/device.h>
#include <device/driver.h>
#include <device/irq.h>				// Pour traitement d'interruption



static dev_t wdt1_dev;
static struct wdt* wdt1;


#ifdef CONFIG_WDT
void reset_watchdog(){

	if(wdt1 == NULL)
		return;


	wdt1->wdt_wdsc |= SOFTRESET;
	while(wdt1->wdt_wdsc & SOFTRESET);

	return;
}
#endif


static int wdt_init(dev_t *dev){


	memcpy(&wdt1_dev, dev, sizeof(dev_t));
	wdt1 = (struct wdt*)(wdt1_dev.base);


	wdt1->wdt_wdsc |= SOFTRESET;
	while(wdt1->wdt_wdsc & SOFTRESET);

	return 0;
}


REGISTER_DRIVER(wdt1, "wdt1,watchdog", wdt_init);
