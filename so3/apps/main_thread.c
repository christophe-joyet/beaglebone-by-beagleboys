/*
 *
 * ----- SO3 Smart Object Oriented (SOO) Operating System -----
 *
 * Copyright (c) 2016,2017 Sootech SA, Switzerland
 * Copyright (c) 2014, 2015, 2016, 2017 REDS Institute, HEIG-VD, Switzerland
 * 
 * This software is released under the MIT License whose terms are defined hereafter.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Contributors:
 *
 * - Daniel Rossier (2015-2017)
 *
 * This file contains some piece of code for various tests in SO3.
 */

#include <thread.h>

//#include <devices/timer.h>

#include <device/timer.h>

extern dm_timer_left(void);
extern dm_timer_ctlr_reg(void);
extern dm_timer_irqstatus_reg(void);
extern dm_periodic_timer_irq_handler(void);
extern intc_irq_disable(unsigned int irq);


int fn1(void *args) {
	volatile int i;
	volatile int k;

	i = 1;

	while (i++){
		k = 0;


		while(k++ < 100000);

		i = 1;
	}


	printk("OUPS SORTIE\n");

	return 0;
}


/*
 * First SO3 main kernel thread
 */
int main_kernel(void *args)
{

	/* Kernel never returns ! */
	printk("***********************************************\n");
	printk("Going to infinite loop...\n");
	printk("Kill Qemu with CTRL-a + x or reset the board\n");
	printk("***********************************************\n");

	kernel_thread(fn1, "fn1", NULL);

 	return 0;
}

