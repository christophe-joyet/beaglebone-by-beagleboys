/*
 * gpios.c
 *
 *  Created on: 13 oct. 2018
 *      Author: root
 */

#include <command.h>
#include <common.h>
#include <stdio.h>
#include <stdlib.h>

//selon manuel AM335X pp. 181-182
#define GPIO0_BASE_ADDRESS		0x44E07000
#define	GPIO1_BASE_ADDRESS	 	0x4804C000
#define GPIO2_BASE_ADDRESS		0x481AC000
#define GPIO3_BASE_ADDRESS		0x481AE000

#define GPIO_REVISION			0x00000000
#define GPIO_SYSCONFIG			0x00000010
#define GPIO_EOI				0x00000020
#define GPIO_IRQSTATUS_RAW_0	0x00000024
#define GPIO_IRQSTATUS_RAW_1	0x00000028
#define GPIO_IRQSTATUS_0		0x0000002C
#define GPIO_IRQSTATUS_1		0x00000030
#define GPIO_IRQSTATUS_SET_0	0x00000034
#define GPIO_IRQSTATUS_SET_1	0x00000038
#define GPIO_IRQSTATUS_CLR_0	0x0000003C
#define GPIO_IRQSTATUS_CLR_1	0x00000040
#define GPIO_IRQWAKEN_0			0x00000044
#define GPIO_IRQWAKEN_1			0x00000048
#define GPIO_SYSSTATUS			0x00000114
#define GPIO_CTRL				0x00000130
#define GPIO_OE					0x00000134
#define GPIO_DATAIN				0x00000138
#define GPIO_DATAOUT			0x0000013C
#define GPIO_LEVELDETECT0		0x00000140
#define GPIO_LEVELDETECT1		0x00000144
#define GPIO_RISINGDETECT		0x00000148
#define GPIO_FALLINGDETECT		0x0000014C
#define GPIO_DEBOUNCENABLE		0x00000150
#define GPIO_DEBOUNCINGTIME		0x00000154
#define GPIO_CLEARDATAOUT		0x00000190
#define GPIO_SETDATAOUT			0x00000194

#define OFFSET_MIN				0x00000000
#define OFFSET_MAX				0x00000194

#define	MAX_ARG	5
#define MIN_ARG 4
#define MAX_GPIO 3
#define MIN_GPIO 0
#define NUMBER_OF_GPIO 26

unsigned int string_to_hex(const char* str, int* err);

static int do_gpios(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[]){

	unsigned int authorized_offset[] = {GPIO_REVISION, GPIO_SYSCONFIG, GPIO_EOI, GPIO_IRQSTATUS_RAW_0, GPIO_IRQSTATUS_RAW_1, GPIO_IRQSTATUS_0, GPIO_IRQSTATUS_1,
										GPIO_IRQSTATUS_SET_0, GPIO_IRQSTATUS_SET_1, GPIO_IRQSTATUS_CLR_0, GPIO_IRQSTATUS_CLR_1, GPIO_IRQWAKEN_0, GPIO_IRQWAKEN_1,
										GPIO_SYSSTATUS, GPIO_CTRL, GPIO_OE, GPIO_DATAIN, GPIO_DATAOUT, GPIO_LEVELDETECT0, GPIO_LEVELDETECT1, GPIO_RISINGDETECT,
										GPIO_FALLINGDETECT, GPIO_DEBOUNCENABLE, GPIO_DEBOUNCINGTIME, GPIO_CLEARDATAOUT, GPIO_SETDATAOUT};
	/*arg0 	: nom de la commande
	* arg1 	: n°GPIO
	* arg2	: read | write
	* arg3	: offset
	* (arg4	: valeur write)
	* */
	int err;
	int i;
	char *read_or_write;
	unsigned int n_gpio; //numéro de la banque GPIO
	unsigned int is_offset_found;
	unsigned int offset;
	unsigned int *address;
	unsigned int val_to_write;


	//verification du nombre d'arguments
	if(argc < MIN_ARG || argc > MAX_ARG){
		printf("invalid number of argument\n");
		return 0;
	}

	//verification du numero de banque de GPIO
	n_gpio = (*argv[1] - '0');
	if(n_gpio < MIN_GPIO || n_gpio > MAX_GPIO){
			printf("invalid number of GPIO\n");
			return 0;
	}

	//verifivation du read ou du write
	read_or_write = argv[2];
	if(*read_or_write != 'r' && *read_or_write != 'w'){
			printf("invalid argument - only 'r' or 'w' authorized\n");
			return 0;
	}

	//verification de l'offset
	offset = string_to_hex(argv[3], &err);
	is_offset_found = 0;
	for(i = 0; i < NUMBER_OF_GPIO; ++i){
		if(authorized_offset[i] == offset){
			is_offset_found = 1;
		}
	}

	if(!(is_offset_found) || (err == -1)){
		printf("invalid offset\n");
		return 0;
	}

	switch(n_gpio){
	case 0 : address = (unsigned int*)(GPIO0_BASE_ADDRESS + offset);
		break;
	case 1 : address = (unsigned int*)(GPIO1_BASE_ADDRESS + offset);
		break;
	case 2 : address = (unsigned int*)(GPIO2_BASE_ADDRESS + offset);
		break;
	case 3 : address = (unsigned int*)(GPIO3_BASE_ADDRESS + offset);
		break;
	default : printf("something goes wrong\n");
	}

	//si on veut écrire une valeur
	if(*read_or_write == 'w'){

		//si pas assez de paramètres
		if(argc < MAX_ARG){
			printf("need value to write\n");
			return 0;
		}

		val_to_write = string_to_hex(argv[4], &err);
		printf("%0x val_to_write\n", val_to_write);

		if(err == -1){
			printf("invalid value to write\n");
			return 0;
		}

		*address = val_to_write;
		printf("%0x write to the address %0x\n", val_to_write, address);
	}else{
		printf("%0x\n", *address);
	}
	return 0;
}


unsigned int string_to_hex(const char* str, int* err){
	unsigned int hex_val;
	char* str_hex;
	int str_hex_length;

	str_hex = str;
	str_hex_length = 0;

	// Etant donné que strlen n'est pas disponible, nous devons déterminer nous même la taille de la string.
	while (*str_hex++) {
		str_hex_length++;
	}

	str_hex = str;

	// On vérifie que la string ait une taille valide
	if(str_hex_length <= 0 || str_hex_length > (sizeof(hex_val)*2)){
		*err = -1;
		return 0;
	}

	hex_val = 0;

	// strtol n'est pas disponible sur u-boot. Il faut donc calculer
	// nous même la valeur hexadécimale du string
	// Nous avons repris et modifié l'algorithem trouvé ici :
	// https://stackoverflow.com/questions/10156409/convert-hex-string-char-to-int
	while (*str_hex) {
		unsigned char str_char = *str_hex++;

		if (str_char >= '0' && str_char <= '9'){
			str_char = str_char - '0';
		}
		else if (str_char >= 'a' && str_char <='f'){
			str_char = str_char - 'a' + 10;
		}
		else if (str_char >= 'A' && str_char <='F'){
			str_char = str_char - 'A' + 10;
		}else{
			// Si  aucun caractère ne corespond à une valeur hexadécimale, nous
			// retournons une erreure
			*err = -1;
			return 0;
		}
		hex_val = (hex_val << 4) | (str_char & 0xF);
	}

	*err = 0;
	return hex_val;
}


U_BOOT_CMD(	gpios,	5,	0,	do_gpios, "Read and write in GPIOS", "<bank number> [r | w] <offset> [value to write]\nBank Number [0-3]\nValue to write must be 1 to 8 length");

